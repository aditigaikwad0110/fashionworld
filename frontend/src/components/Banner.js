import React from "react";
import { Flex, Heading, Image, Text, Button } from "@chakra-ui/react";

const Banner = () => {
  return (
    <>
      <Flex
        bgColor="gray.100"
        mt="2px"
        height="400px"
        width="100%"
        justifyContent="space-around"
        alignItems="center"
       
      >
        <Heading color="Black" textTransform="uppercase">
          let's create your own style
          <Text color="teal.500" >Get the Trendy Bag.. </Text>
          <Button
            mt="30px"
            padding="20px"
            colorScheme="red"
            _hover={{ bgColor: "black", color: "white" }}
            fontSize='15px'
            borderRadius='15px'

          >
            Shop now
          </Button>
        </Heading>

        <Image
          src="./images/banner.png"
          objectFit="contain"
          boxSize={{ base: "300px", md: "400px" }}
        />
      </Flex>
    </>
  );
};

export default Banner;