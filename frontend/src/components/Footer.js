import React from "react";
import { Flex, Text } from "@chakra-ui/react";


const Footer = () => {
  return (
    <Flex
      as="Footer"
      justifyContent="center"
      py="5"
      bgColor="black"
      color="white"
    >
      <Text>
        Copyright {new Date().getFullYear()}. RST STORE. ©All Right Reserved.
      </Text>
    </Flex>
  );
};

export default Footer;