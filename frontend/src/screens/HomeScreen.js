import { Grid, Heading } from "@chakra-ui/react";
import ProductCards from "../components/ProductCards";
import Banner from "../components/Banner";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "../actions/productActions";
import Loader from "../components/Loader";
import Message from "../components/Message";

const HomeScreen = () => {
  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;

  useEffect(() => {
    dispatch(listProducts());
  }, [dispatch]);

  return (
    <>
      <Banner />

      <Heading
        as="h2"
        fontWeight="bold"
        mt="30px"
        mb="8"
        fontSize="2xl"
        textAlign="center"
        color="red"
        textDecor="underline"
      >
        Latest Collection
      </Heading>

      { loading ? (
        <Loader/>
      ) : error ? (
       <Message type='error'>{error}</Message>
      ) : (
        <Grid
          templateColumns={{
            base: "1fr",
            md: "1fr 1fr 1fr",
            lg: "1fr 1fr 1fr 1fr",
          }}
          gap="8"
          m="5"
        >
          {products.map((prod) => (
            <ProductCards key={prod._id} product={prod} />
          ))}
        </Grid>
      )}
    </>
  );
};

export default HomeScreen;
