import express from "express";
import {
  authUser,
  getUserProfile,
  registerUser,
  getUsers,
  getUserByID,
  updateUserProfile,
  updateUser,
  deleteUser
} from "../controllers/userController.js";
import { protect, admin } from "../middlewares/authMiddleware.js";

const router = express.Router();

router.route("/").post(registerUser);
router.route("/").post(registerUser).get(protect, admin, getUsers);
router.route("/login").post(authUser);
router.route("/profile").get(protect, getUserProfile);
router.route("/profile").put(protect, updateUserProfile);
router.route("/:id").delete(protect, admin, deleteUser);
router.route("/:id").delete(protect, admin, getUserByID);
router.route("/:id").delete(protect, admin, updateUser);


export default router;
