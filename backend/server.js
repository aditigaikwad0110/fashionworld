import dotenv from 'dotenv';
import express from 'express';
import colors from 'colors';
import connectDB from './config/db.js';
import path from 'path';
import { errorHandler, notFound } from './middlewares/errorMiddleware.js';
import productRoutes from './routes/productRoutes.js';
import userRoutes from './routes/userRoutes.js';
import uploadRoutes from './routes/uploadRoutes.js';
import orderRoutes from './routes/orderRoutes.js';

dotenv.config();

connectDB();

const app = express ();
app.use(express.json());  //request body parsing;

//routes in express
app.get('/', (req, res) => {
    res.send("API is running....");
});

app.use('/api/products', productRoutes);
app.use('/api/users', userRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/uploads', uploadRoutes);

// Create a static folder
const __dirname = path.resolve();
app.use(`/uploads`, express.static(path.join(__dirname, '/uploads')));




//always put this 2 file at end 
app.use(notFound);
app.use(errorHandler);

//calling the app (running express)
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold);
})