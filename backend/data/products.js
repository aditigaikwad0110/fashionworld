const products = [
  {
    name: "Women Floral Print Lounge T-Shirt",
    image: "/images/female_img_3.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Burberry",
    category: "Women Fashion",
    price: 12000,
    countInStock: 22,
    rating: 2.6,
    numReviews: 4,
  },
  {
    name: "Bag",
    image: "/images/bag.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Urban Outfitters",
    category: "Women Fashion",
    price: 1500,
    countInStock: 42,
    rating: 1.5,
    numReviews: 2,
  },
  {
    name: "Rolex Men Watch",
    image: "/images/men-watch.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Men Fashion",
    price: 3000,
    countInStock: 2,
    rating: 4.2,
    numReviews: 9,
  },
  {
    name: "Royal Blue Shirt",
    image: "/images/shirt.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 9500,
    countInStock: 20,
    rating: 4.2,
    numReviews: 5,
  },
  {
    name: "Men Formal Shoes",
    image: "/images/men-shoes.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Men Fashion Shoes",
    price: 4500,
    countInStock: 3,
    rating: 4.2,
    numReviews: 5,
  },
  {
    name: "Juventus Henley Neck Jersey",
    image: "/images/male_img_1.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Tom Ford",
    category: "Men Fashion",
    price: 16000,
    countInStock: 21,
    rating: 4.1,
    numReviews: 3,
  },
  {
    name: "DW Watches with Braclet",
    image: "/images/women-watch.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 30000,
    countInStock: 10,
    rating: 5,
    numReviews: 5,
  },
  {
    name: "Printed Polo Collar T-shirt",
    image: "/images/female_img_4.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 9500,
    countInStock: 0,
    rating: 4.2,
    numReviews: 5,
  },

  {
    name: "Crochet Detail Lightweight Top",
    image: "/images/female_img_2.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 43000,
    countInStock: 6,
    rating: 4.9,
    numReviews: 8,
  },
  {
    name: "Heels",
    image: "/images/Women-heel.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 4500,
    countInStock: 10,
    rating: 3.6,
    numReviews: 5,
  },
  {
    name: "Native Youth",
    image: "/images/male_img_2.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Ralph Lauren",
    category: "Men Fashion",
    price: 6000,
    countInStock: 18,
    rating: 4.5,
    numReviews: 2,
  },
];

export default products;
